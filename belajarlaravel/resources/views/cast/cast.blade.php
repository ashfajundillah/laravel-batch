@extends('layout.master')
@section('content')

<a href="/cast/create" class="btn btn-primary mb-3">Tambah</a>

<table class="table table-bordered table-dark">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $value)
            
        <tr>
          <th scope="row">{{$key +1}}</th>
          <td>{{$value->nama}}</td>
          <td>{{$value->umur}}</td>
          <td>{{$value->bio}}</td>

          <td>
            <a href="/cast/{{$value->id}}" class="btn btn-info">Show</a>
            <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
            <form action="/cast/{{$value->id}}" method="POST">
                @csrf
                @method('DELETE')
                <input type="submit" class="btn btn-danger my-1" value="Delete">
            </form>
        </td>

        </tr>
        @empty
            
        @endforelse
      
    </tbody>
  </table>
    
@endsection