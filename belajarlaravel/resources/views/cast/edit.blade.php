@extends('layout.master')
@section('content')
  
        <form action="/cast/{{$cast->id}}" method="post">
            @csrf
            @method('PUT')
            <div class="form-group">
             <label for="exampleInputEmail1">nama</label>
            <input type="text" name='nama'class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$cast->nama}}">
            </div>
            @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <div class="form-group">
            <label for="exampleInputPassword1">umur</label>
            <input type="number" name='umur'class="form-control" id="exampleInputPassword1" value="{{$cast->umur}}">
            </div>
            @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <div class="form-group">
                <label for="exampleInputPassword1">bio</label>
                <textarea name="bio" class="form-control" id="" cols="30" rows="10">{{$cast->bio}}</textarea>
                </div>
                @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    
@endsection