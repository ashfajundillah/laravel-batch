@extends('layout.master')
@section('content')
  
        <form action="/cast" method="post">
            @csrf
            <div class="form-group">
             <label for="exampleInputEmail1">nama</label>
            <input type="text" name='nama'class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
            </div>
            @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <div class="form-group">
            <label for="exampleInputPassword1">umur</label>
            <input type="number" name='umur'class="form-control" id="exampleInputPassword1">
            </div>
            @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <div class="form-group">
                <label for="exampleInputPassword1">bio</label>
                <textarea name="bio" class="form-control" id="" cols="30" rows="10"></textarea>
                </div>
                @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    
@endsection