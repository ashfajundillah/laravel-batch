@extends('layout.master')
@section('content')
       
    <div class="card text-center">
        <div class="card-header">
        {{$cast->nama}}
        </div>
        <div class="card-body">
        <h5 class="card-title">{{$cast->umur}}</h5>
        <p class="card-text">{{$cast->bio}}</p>
        </div>
    </div>
    
@endsection