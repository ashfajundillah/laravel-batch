<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;
use App\http\Controllers\AthoController;
use App\http\Controllers\castcontroller;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);

Route::get('/from', [AthoController::class, 'link']);

Route::post('/kirim', [AthoController::class, 'kirim']);

Route::get('/master', function(){
    return view('layout.master');
});
Route::get('/table', function(){
    return view('layout.table');
});
Route::get('/datatable', function(){
    return view('layout.datatable');
});

//CRUD cast
//create
// form tambah cast 

Route::get('/cast', [castcontroller::class, 'getdatacast']);

Route::get('/cast/create', [castcontroller::class, 'create']);

Route::post('/cast', [castcontroller::class, 'store']);

Route::get('/cast/{cast_id}', [castcontroller::class, 'show']);

Route::get('/cast/{cast_id}/edit', [castcontroller::class, 'edit']);

Route::put('/cast/{cast_id}', [castcontroller::class, 'update']);


Route::delete('/cast/{cast_id}', [castcontroller::class, 'destroy']);






